function login(userid,pwd){
	var jsonA = {username:userid,password:pwd,loginType:'admin'};
	$.ajax({
		url:apis.login_url,
		type:'post',
		data: JSON.stringify(jsonA),
		dataType:'json',
		contentType: 'application/json;charset=utf-8',
		success:function(msg){
			if(msg.code==1){
				closePwdError(msg.token);
                //window.location.href = "adminIndex.html";
			}else if(msg.code==-1){
				showPwdError();
			}
		},
		error:function(){
			showPwdError();
			  
		}
	});
}

function showPwdError(){
	$('#password').popover({
		content: '用户名或密码错误',
		theme: 'danger sm'
	});
	$('#password').popover('open')
}

/**
 * 登录成功。。跳转到index页面，并且设置cookie
 * @returns
 */
function closePwdError(token){
	var rememberme=$('input:checkbox:checked').val();//选择记住我，记录到cookie
	if(rememberme==1){
		setPassword();
		setCookie(apis.rememberme, 1);
	}else{
		setCookie(apis.rememberme, 0);
		setCookie(apis.password, '');
	}
	setAccountid();
	setToken(token);
	$('#password').popover('close');
	window.location.href = "index.html";
}


/**
 * 设置用户cookie
 * @returns
 */
function setAccountid(){
	setCookie(apis.accountid, $('#accountid').val());
}

/**
 * 设置密码cookie
 * @returns
 */
function setPassword(){
	setCookie(apis.password, $('#password').val());
}

/**
 * 设置token cookie
 * @returns
 */
function setToken(value){
	setCookie(apis.token, value);
}

/**
 * 获取用户cookie
 * @returns
 */
function getAccountid(){
	return getCookie(apis.accountid);
}

/**
 * 获取密码cookie
 * @returns
 */
function getPassword(){
	return getCookie(apis.password);
}

/**
 * 获取token cookie
 * @returns
 */
function geToken(){
	return getCookie(apis.token);
}


/**
 * 设置cookie，有效期
 * @param name：
 * @param value：null 表示删除cookie值
 * @param daylong
 * @returns
 */
function setCookie(name,value,daylong){
	if(typeof(daylong)=="undefined"){
		daylong=1;
	}
	$.cookie(name, value, { expires: daylong });
}

/**
 * 
 * @param name
 * @returns
 */
function getCookie(name){
	return $.cookie(name);
}

function setLoginValue(){
	$('#accountid').val(getAccountid());
	$('#password').val(getPassword());
	var rememberme=getCookie(apis.rememberme);
	if(rememberme==1){
		$("#remember-me").prop("checked",true);
	}else{
		$("#remember-me").prop("checked",false);
	}
}

/**
 * 如果在登录界面检测到登录状态。。跳转到index页面
 * @returns
 */
function checkLogin(){
	var token=geToken();
	if(!(token==null||token=='')){
		window.location.href = "index.html";
	}else{
		//window.location.href = "login.html";
	}
}

/**
 * 如果在登录界面检测到登录状态。。跳转到login页面
 * @returns
 */
function checkLoginOtherPage(){
	var token=geToken();
	if(!(token==null||token=='')){
		//window.location.href = "index.html";
	}else{
		window.location.href = "login.html";
	}
}

/**
 * 如果在登录界面检测到登录状态。。跳转到index页面
 * @returns
 */
function loginout(){
	setToken('');
	setCookie(apis.accountid, '');
	window.location.href = "login.html";
}

/**获取URL参数**/
function getQueryVariable(variable)
{
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i=0;i<vars.length;i++) {
        var pair = vars[i].split("=");
        if(pair[0] == variable){return pair[1];}
    }
    return(false);
}

/**
 * 获取image对象的base64
 * @param imageId控件id
 * @returns base64编码
 */
function getBase64Image(imageId) {
	var img=document.getElementById(imageId);
    var canvas = document.createElement("canvas");
    canvas.width = img.width;
    canvas.height = img.height;
    try
    {
    var ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0, img.width, img.height);
    var dataURL = "";
    
    	dataURL=canvas.toDataURL("image/png");
    }
  catch(err)
    {
	  //在这里处理错误
    }
  	if(typeof(dataURL)=="undefined"){
		dataURL="";
	}
    var result=dataURL.replace("data:image/png;base64,", "").replace("data:image/jpeg;base64,","");
    return result;
}


/**
 * 获取image对象的base64
 * @param imageId控件id
 * @returns base64编码
 */
function getFileImage(filechooser,previewer) {
	var fch = document.getElementById(filechooser);//此处不支持jQuery选择器
	var files = fch.files;
    var file = files[0];
    if (!/\/(?:jpeg|jpg|png)/i.test(file.type)) {
    	alert('仅支持jpeg、jpg、png格式');
    	return;
    }//非图片类
    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function() {
       var result = this.result;
       $("#"+previewer).html("<img src='"+result+"' id='previewer' style='max-height: 800px;max-width: 400px' alt='预览图'/>");
       $("img.am-img-loaded").attr("src",result);
    };
}


function initValidator(formid){
	var form = $('#'+formid);
	form.validator({
		 onValid: function(validity) {
		      $(validity.field).closest('.am-u-sm-9').find('.am-alert').hide();
		    },
		 onInValid: function(validity) {
		        var $field = $(validity.field);
		        var $group = $field.closest('.am-u-sm-9');
		        var $alert = $group.find('.am-alert');
		        // 使用自定义的提示信息 或 插件内置的提示信息
		        var msg = $field.data('validationMessage') || this.getValidationMessage(validity);
		        if (!$alert.length) {
		          	$alert = $('<div class="am-alert am-alert-danger"></div>').hide().
		            appendTo($group);
		        }
		        $alert.html(msg).show();
		 }
	});
	return form;
	//var abc=form.validator('isFormValid');
	//AMUI.dialog.confirm({ title: '确认提示', content: '请在点击确定前牢记修改后的密码？', onConfirm: function() { labelSubmit() }, onCancel: function() { console.log('onCancel') } });
}

function validatorForm(formid){
	return initValidator(formid).validator('isFormValid');
}


