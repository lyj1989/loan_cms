/**
 * 引用JS和CSS头文件
 */
var rootPath = getRootPath(); //项目路径

/**
 * 动态加载CSS和JS文件
 */
var dynamicLoading = {
    meta : function(){
        document.write('<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">');
    },
    css: function(path){
        if(!path || path.length === 0){
            throw new Error('argument "path" is required!');
        }
        document.write('<link rel="stylesheet" type="text/css" href="' + path + '">');
    },
    js: function(path, charset){
        if(!path || path.length === 0){
            throw new Error('argument "path" is required!');
        }
        document.write('<script charset="' + (charset ? charset : "utf-8") + '" src="' + path + '"></script>');
    }
};

/**
 * 取得项目路径
 * @author wul
 */
function getRootPath() {
    //取得当前URL
    var path = window.document.location.href;
    //取得主机地址后的目录
    var pathName = window.document.location.pathname;
    var post = path.indexOf(pathName);
    //取得主机地址
    var hostPath = path.substring(0, post);
    //取得项目名
    var name = pathName.substring(0, pathName.substr(1).indexOf("/") + 1);
    return hostPath + name + "/";
}

//动态生成meta
dynamicLoading.meta();

//引入jQuery JS库
dynamicLoading.js("https://cdn.bootcss.com/jquery/3.3.1/jquery.min.js", "utf-8");
dynamicLoading.js("https://cdn.bootcss.com/jquery-cookie/1.4.1/jquery.cookie.min.js", "utf-8");


//引入amazeui JS库以及CSS库
dynamicLoading.js("http://cdn.amazeui.org/amazeui/2.7.2/js/amazeui.min.js", "utf-8");
dynamicLoading.js("http://cdn.amazeui.org/amazeui/2.7.2/js/amazeui.ie8polyfill.min.js", "utf-8");
dynamicLoading.js("http://cdn.amazeui.org/amazeui/2.7.2/js/amazeui.widgets.helper.min.js", "utf-8");
//dynamicLoading.css("http://cdn.amazeui.org/amazeui/2.7.2/css/amazeui.css");
dynamicLoading.css("http://cdn.amazeui.org/amazeui/2.7.2/css/amazeui.min.css");

dynamicLoading.js("https://cdn.bootcss.com/moment.js/2.22.0/moment.min.js", "utf-8");
dynamicLoading.js("https://cdn.bootcss.com/moment.js/2.22.0/locale/zh-cn.js", "utf-8");



dynamicLoading.js("amazeui/js/amazeui.datatables.js", "utf-8");
dynamicLoading.js("amazeui/js/dataTables.responsive.min.js", "utf-8");
dynamicLoading.js("amazeui/js/app.js", "utf-8");

dynamicLoading.js("amazeui/js/amazeui.tree.min.js", "utf-8");
dynamicLoading.js("amazeui/js/amazeui.dialog.js", "utf-8");
dynamicLoading.js("amazeui/js/amazeui.chosen.min.js", "utf-8");

dynamicLoading.js("https://cdn.bootcss.com/lightbox2/2.10.0/js/lightbox.min.js", "utf-8");


dynamicLoading.css("amazeui/css/amazeui.datatables.min.css");
dynamicLoading.css("amazeui/css/amazeui.chosen.css");
dynamicLoading.css("amazeui/css/app.css");
dynamicLoading.css("amazeui/css/admin.css");

dynamicLoading.css("amazeui/css/amazeui.tree.min.css");

dynamicLoading.css("https://cdn.bootcss.com/lightbox2/2.10.0/css/lightbox.min.css");

	
	



