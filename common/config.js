/**
 * 公共URL或常量
 */
var baseurl ="http://api.sinmalls.com:9002/";//http://api.sinmalls.com:9002/   http://127.0.0.1:8010/  http://39.108.247.79:9010/
var apis = {
	"accountid":"accountid",
	"password":"password",
	"token":"token",
	"rememberme":"rememberme",
    //登陆
    "login_url":baseurl+"/account/login",
    "modifpwd_url":baseurl+"/account/modifpwd",
    //首页
    
    //banner
    "bannerList_url":baseurl+"/banner/getList",
    "bannerKey_url":baseurl+"/banner/getKey",
    "bannerMove_url":baseurl+"/banner/move",
    "bannerOnline_url":baseurl+"/banner/online",
    "bannerUpdate_url":baseurl+"/banner/update",
    "bannerInsert_url":baseurl+"/banner/insert",
    
    "labelList_url":baseurl+"/label/getList",
    "labelKey_url":baseurl+"/label/getKey",
    "labelMove_url":baseurl+"/label/move",
    "labelOnline_url":baseurl+"/label/online",
    "labelUpdate_url":baseurl+"/label/update",
    "labelInsert_url":baseurl+"/label/insert",

    "noticeList_url":baseurl + "/notice/getList",
    "noticeKey_url":baseurl + "/notice/getKey",
    "noticeinsert_url":baseurl + "/notice/insert",
    "noticeupdate_url":baseurl + "/notice/update",
    "noticeOnline_url":baseurl + "/notice/online",

    "conpanyList_url":baseurl + "/company/getList",
    "conpanyOnline_url":baseurl + "/company/online",
    "conpanyInsert_url":baseurl + "/company/insert",
    "conpanyUpdate_url":baseurl + "/company/update",
    
    "productList_url":baseurl+"/product/getList",
    "productKey_url":baseurl+"/product/getKey",
    "productUpdate_url":baseurl+"/product/update",
    "productInsert_url":baseurl+"/product/insert",
    "productUpdateOrder_url":baseurl+"/product/updateOrder",
    "productOnline_url":baseurl+"/product/online",
    
    "loanopList_url":baseurl+"/loanop/getList",
    "loanopUpdate_url":baseurl+"/loanop/update",
    "loanopInsert_url":baseurl+"/loanop/insert",
    "loanopOnline_url":baseurl+"/loanop/online",
    
    "mailUpdate_url":baseurl+"/mail/update",
    "mailInsert_url":baseurl+"/mail/insert",
    "mailDelete_url":baseurl+"/mail/delete",
    "mailList_url":baseurl+"/mail/getList",
    
    "clientInsert_url":baseurl+"/clientconfig/insert",
    "clientUpdate_url":baseurl+"/clientconfig/online",
    "clientList_url":baseurl+"/clientconfig/getOnlineStatus",

    "echarRegiste_url":baseurl+'/echart/register',
    "echarApply_url":baseurl+'/echart/apply',

    "userList_url":baseurl+'/account/queryAccountList',

    "poptionsList_url":baseurl+'/poptions/getList',
    "poptionsUpdate_url":baseurl+'/poptions/update',
    "poptionsInsert_url":baseurl+'/poptions/insert',
    "poptionsDelete_url":baseurl+'/poptions/delete',
    
    "visitsList_url":baseurl+'/visits/getList',

    "channelInsert_url":baseurl+"/cnode/insert",
    "channelUpdate_url":baseurl+"/cnode/update",
    "channelList_url":baseurl+"/cnode/getList",

    "plinkInsert_url":baseurl + "/plink/insert",
    "plinkUpdate_url":baseurl + "/plink/update",
    "plinkList_url":baseurl + "/plink/getList",
    
    "activityInsert_url":baseurl + "/redmoney/admin/insertActivity",
    "activityUpdate_url":baseurl + "/redmoney/admin/updateActivity",
    "activityList_url":baseurl + "/redmoney/admin/getListActivity",
    "fetchmoneyList_url":baseurl + "/redmoney/admin/getListFetchmoney",
    "fetchmoneyApproval_url":baseurl + "/redmoney/admin/approval",
    "fetchmoneyFetch_url":baseurl + "/redmoney/admin/fetch",
    
    "loaninfoList_url":baseurl + "/loaninfo/getLoaninfoList",
    "loaninfoExport_url":baseurl + "/loaninfo/export",
    
    "mpconfigInsert_url":baseurl + "/mpconfig/insertMpconfig",
    "mpconfigUpdate_url":baseurl + "/mpconfig/updateMpconfig",
    "mpconfigList_url":baseurl + "/mpconfig/getMpconfigList",
    
    "tagInsert_url":baseurl + "/tag/insert",
    "tagUpdate_url":baseurl + "/tag/update",
    "tagList_url":baseurl + "/tag/getList",
    "tagListAll_url":baseurl + "/tag/getListAll",
}